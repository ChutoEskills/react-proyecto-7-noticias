# React Proyecto 7 - Noticias
Obtención de noticias de un país, mediante API.

[Link to Web:](https://dreamy-ardinghelli-f43d1f.netlify.app/)

## ¿Herramientas utilizadas?
- newsapi.org
- Materialize
- useState
- Hooks
- Ajax Request/Promises


## ¿Cómo funciona?
- Se especifica el país de México para posteriormente enviar una consulta como POST y obtener las noticias mas relevantes en la categoría selecionada.


## ¿Dudas?
